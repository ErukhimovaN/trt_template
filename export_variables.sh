#!/usr/bin/bash

export MODEL_PATH=yolox_p_d_dynam_fp32.trt
export DATASET_DIR=datasets/minival/minival
export OUTPUT_DIR=datasets/minival/vis_res_trt
export ANNOTATION_DIR=datasets/minival/annotations/minival.json
export BATCH_SIZE=1
export CONF_THR=0.01
export NMS_THR=0.65