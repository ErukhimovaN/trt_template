import collections
import contextlib
import hashlib
import logging
import os
import pathlib
from typing import List, Tuple, Union

import torch
import torchvision
from torch.nn.functional import interpolate
import tensorrt as trt

import trt_utils

LOG = logging.getLogger(__name__)


@contextlib.contextmanager
def null_cn(*args, **kwargs):
    yield


OptProfileConfig = collections.namedtuple('OptProfileConfig',
                                          ['input_name', 'min', 'opt', 'max'])


def build_engine(logger, model_path, max_batch_size=1, opt_profile=False):
    builder = trt.Builder(logger)
    network = builder.create_network(
        flags=1 << int(trt.NetworkDefinitionCreationFlag.EXPLICIT_BATCH))
    parser = trt.OnnxParser(network, logger)

    success = parser.parse_from_file(str(model_path))
    for idx in range(parser.num_errors):
        print(parser.get_error(idx))

    if not success:
        raise RuntimeError(f"Can't load {model_path}")

    config = builder.create_builder_config()

    # use FP16 mode if possible
    if builder.platform_has_fast_fp16:
        print("fp16 mode enabled")
        config.set_flag(trt.BuilderFlag.FP16)
    config.max_workspace_size = 1 << 30  # 1 GiB

    builder.max_batch_size = max_batch_size

    if opt_profile:
        assert network.num_inputs == 1, "Network with many inputs is unsupported"
        net_input = network.get_input(0)
        # c, h, w
        shape = net_input.shape[1:]
        profile = builder.create_optimization_profile()
        profile.set_shape(input=net_input.name,
                          min=(1, ) + shape,
                          opt=(max_batch_size, ) + shape,
                          max=(max_batch_size, ) + shape)
        config.add_optimization_profile(profile)

    serialized_engine = builder.build_serialized_network(network, config)
    return serialized_engine


def get_serialized_engine(
    logger,
    model_path: pathlib.Path,
    cache_path,
    max_batch_size=1,
    opt_profile=False,
) -> bytes:

    checksum = hashlib.md5(model_path.read_bytes()).hexdigest()
    if model_path.suffix=='.trt':
        print('Found trt engine in the model_path')
        engine_filename = model_path
    else:
        print('Looking for engine in cache')
        engine_filename = pathlib.Path(
            cache_path) / f'{model_path.stem}_bs_{max_batch_size}_{checksum}.trt'
        engine_filename.parent.mkdir(exist_ok=True, parents=True)
    if not engine_filename.exists():
        print('Trt engine not found, converting onnx model')
        serialized_engine = build_engine(logger, model_path, max_batch_size,
                                         opt_profile)
        engine_filename.write_bytes(serialized_engine)
        print("TRT engine was saved to ", engine_filename)
    return engine_filename.read_bytes()


def get_resized_crops(frame, bboxes, ids, shape, dtype) -> torch.Tensor:
    assert len(bboxes) == len(ids)
    crops = []
    remain_ids = []
    for bbox, item_id in zip(bboxes, ids):
        crop = trt_utils.safe_crop_frame(frame, bbox).unsqueeze(0).to(dtype)
        if crop.numel() == 0:
            continue
        crop = interpolate(crop,
                           shape,
                           mode='bilinear',
                           align_corners=False)
        crops.append(crop)
        remain_ids.append(item_id)
    crops = torch.cat(crops, dim=0)
    return crops, remain_ids


class TRTModel:
    cache_path = os.environ.get('TRT_CACHE', '/tmp/trt_cache')
    name = "base"
    # Single instance of logger for every model
    trt_logger = trt.Logger(trt.Logger.INFO)

    def __init__(
        self,
        onnx_model_path,
        batch_size,
        opt_profile=False,
    ):

        serialized_engine = get_serialized_engine(self.trt_logger,
                                                  onnx_model_path,
                                                  self.cache_path, batch_size,
                                                  opt_profile)

        runtime = trt.Runtime(self.trt_logger)
        self.engine = runtime.deserialize_cuda_engine(serialized_engine)
        self.input_shape: Tuple[int, int]

        avail_bindings = [binding for binding in self.engine]
        LOG.info(f"Available model bindings: {avail_bindings}")

        self.context = self.engine.create_execution_context()
        self.outputs = []
        self.bindings = []
        self.stream = torch.cuda.Stream()  # type: ignore
        self.device: torch.device
        self.allocate_buffers_torch(batch_size)
        self.batch_size = batch_size

    def allocate_buffers_torch(self, batch_size):
        self.dtypes = []
        for idx, binding in enumerate(self.engine):
            binding_dtype = self.torch_dtype_from_trt(
                self.engine.get_binding_dtype(binding))
            self.dtypes.append(binding_dtype)
            self.device = self.torch_device_from_trt(
                self.engine.get_location(binding))

            shape = tuple(self.engine.get_binding_shape(binding))
            if self.engine.binding_is_input(binding):
                self.bindings.append(-1)
                self.input_shape = shape[-2:]
            else:
                shape = (batch_size, ) + shape[1:]
                device_mem = torch.empty(size=shape,
                                         dtype=binding_dtype,
                                         device=self.device)
                self.bindings.append(device_mem.data_ptr())
                self.outputs.append(device_mem)

    def process_frame(self, frame: torch.Tensor, timer=None):
        timer = timer or null_cn
        with timer(f'{self.name}_preprocess'):
            with torch.cuda.stream(self.stream):
                preprocessed = self.preprocess(frame)
        with timer(f'{self.name}_inference'):
            inferred = self.inference(preprocessed)
        with timer(f'{self.name}_postprocess'):
            detections = self.postprocess(inferred)
        return detections

    def preprocess(self, frame: torch.Tensor) -> torch.Tensor:
        raise NotImplementedError

    def inference(self, preprocessed: torch.Tensor):
        raise NotImplementedError

    def postprocess(self, inferred):
        raise NotImplementedError

    def do_inference_torch(self):
        self.stream.synchronize()

        self.context.execute_async_v2(bindings=self.bindings,
                                      stream_handle=self.stream.cuda_stream)

        self.stream.synchronize()
        return tuple(self.outputs)

    def get_device(self) -> torch.device:
        return self.device

    @staticmethod
    def torch_dtype_from_trt(dtype):
        if dtype == trt.int8:
            return torch.int8
        elif trt.__version__ >= "7.0" and dtype == trt.bool:
            return torch.bool
        elif dtype == trt.int32:
            return torch.int32
        elif dtype == trt.float16:
            return torch.float16
        elif dtype == trt.float32:
            return torch.float32
        else:
            raise TypeError("%s is not supported by torch" % dtype)

    @staticmethod
    def torch_device_from_trt(device) -> torch.device:
        if device == trt.TensorLocation.DEVICE:
            return torch.device("cuda")
        elif device == trt.TensorLocation.HOST:
            return torch.device("cpu")
        else:
            raise TypeError("%s is not supported by torch" % device)



class YoloXNanoModel(TRTModel):

    name = "detector"

    def __init__(
        self,
        engine_path,
        batch_size,
        nms_thr=0.7,
        score_thr=0.3,
    ):
        super().__init__(engine_path, batch_size, opt_profile=True)
        self.nms_thr = nms_thr
        self.score_thr = score_thr


    def preprocess(self, image: torch.Tensor) -> torch.Tensor:
        h,w = image.shape[-2:]
        self.orig_w = w
        self.orig_h = h
        self.ratio_x = w / self.input_shape[1]
        self.ratio_y = h / self.input_shape[0]
        ratio = torch.tensor([self.ratio_x,self.ratio_y,self.ratio_x,self.ratio_y])
        image = interpolate(image.unsqueeze(0),
                             self.input_shape,
                             mode='bilinear',
                             align_corners=False)
        return image, ratio
    

    def inference(self, preprocessed: torch.Tensor):
        self.context.set_binding_shape(0,
                                       trt.tensorrt.Dims(preprocessed.shape))
        self.bindings[self.engine["images"]] = preprocessed.data_ptr()
        outputs,  = self.do_inference_torch()
        return outputs

    def postprocess(self, prediction, num_classes=80) -> dict:
        # prediction, =prediction
        box_corner = prediction.new(prediction.shape)
        box_corner[:, :, 0] = prediction[:, :, 0] - prediction[:, :, 2] / 2
        box_corner[:, :, 1] = prediction[:, :, 1] - prediction[:, :, 3] / 2
        box_corner[:, :, 2] = prediction[:, :, 0] + prediction[:, :, 2] / 2
        box_corner[:, :, 3] = prediction[:, :, 1] + prediction[:, :, 3] / 2
        prediction[:, :, :4] = box_corner[:, :, :4]

        output = [None for _ in range(len(prediction))]
        for i, image_pred in enumerate(prediction):

            # If none are remaining => process next image
            if not image_pred.size(0):
                continue
            # Get score and class with highest confidence
            class_conf, class_pred = torch.max(image_pred[:, 5: 5 + num_classes], 1, keepdim=True)

            conf_mask = (image_pred[:, 4] * class_conf.squeeze() >= self.score_thr).squeeze()
            # Detections ordered as (x1, y1, x2, y2, obj_conf, class_conf, class_pred)
            detections = torch.cat((image_pred[:, :5], class_conf, class_pred.float()), 1)
            detections = detections[conf_mask]
            if not detections.size(0):
                continue

            nms_out_index = torchvision.ops.batched_nms(
                detections[:, :4],
                detections[:, 4] * detections[:, 5],
                detections[:, 6],
                self.nms_thr,
            )

            detections = detections[nms_out_index]
            if output[i] is None:
                output[i] = detections
            else:
                output[i] = torch.cat((output[i], detections))

        return output

