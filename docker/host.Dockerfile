FROM nvcr.io/nvidia/tensorrt:23.05-py3

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        python3-pip \
        libgl1\
    && rm -rf /var/lib/apt/lists

RUN python3 -m pip install -U \
        pip \
        setuptools

WORKDIR /demo
COPY . /demo

RUN python3 -m pip install --no-cache --prefer-binary -r ./requirements.txt 

