FROM nvcr.io/nvidia/l4t-base:r32.7.1

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        python3-pip \
        libgl1\
        libfreetype6-dev\
        libpng-dev\
        pkg-config\ 
        git\
        libopenblas-base\
        libopenmpi-dev\
        libomp-dev\
    && rm -rf /var/lib/apt/lists

RUN python3 -m pip install -U \
        pip \
        setuptools

RUN apt-get update && apt-get install -y \
    build-essential \
    python3-dev \
    && rm -rf /var/lib/apt/lists/*

RUN wget https://nvidia.box.com/shared/static/p57jwntv436lfrd78inwl7iml6p13fzh.whl \
         -O torch-1.8.0-cp36-cp36m-linux_aarch64.whl \
         && pip3 install Cython \
         && pip3 install numpy torch-1.8.0-cp36-cp36m-linux_aarch64.whl --no-cache \
         && rm torch-1.8.0-cp36-cp36m-linux_aarch64.whl

WORKDIR /demo
COPY . /demo

RUN python3 -m pip install --no-cache --prefer-binary -r ./requirements_jetson.txt 
RUN pip3 install --no-cache pycocotools