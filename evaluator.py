#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# Copyright (c) Megvii, Inc. and its affiliates.

# This file is customised version of yolox/evaluators/coco_evaluator.py

import contextlib
import io
import json
import tempfile

from utils import xyxy2xywh
from pycocotools.coco import COCO

class_ids = [1,2,3,4,5,6,7,8,9,10,11,13,14,15,16,17,18,19,20,21,22,23,24,25,27,28,
             31,32,33,34,35,36,37,38,39,40,41,42,43,44,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,
             63,64,65,67,70,72,73,74,75,76,77,78,79,80,81,82,84,85,86,87,88,89,90]


def evaluate_prediction(data_dict, path_to_annotation):
    '''
    Returns (AP50_95, AP50)
    '''

    annType = ["segm", "bbox", "keypoints"]

    # Evaluate the Dt (detection) json comparing with the ground truth
    if len(data_dict) > 0:
        cocoGt = COCO(path_to_annotation)

        _, tmp = tempfile.mkstemp()
        json.dump(data_dict, open(tmp, "w")) 
        cocoDt = cocoGt.loadRes(tmp)
        try:
            from yolox.layers import COCOeval_opt as COCOeval
        except ImportError:
            from pycocotools.cocoeval import COCOeval

        cocoEval = COCOeval(cocoGt, cocoDt, annType[1])
        cocoEval.evaluate()
        cocoEval.accumulate()
        redirect_string = io.StringIO()
        with contextlib.redirect_stdout(redirect_string):
            cocoEval.summarize()

        return cocoEval.stats[0], cocoEval.stats[1]
    else:
        return 0, 0




def convert_to_coco_format(data_list, bboxes, scores, cls, image_path, annotations_data, class_ids = class_ids):

    bboxes = xyxy2xywh(bboxes)

    for image in annotations_data['images']:
        if image['file_name'] == str(image_path.name):
            image_id = image['id']

    for ind in range(bboxes.shape[0]):
        label = class_ids[int(cls[ind])]
        pred_data = {
            "image_id": int(image_id),
            "category_id": label,
            "bbox": bboxes[ind].numpy().tolist(),
            "score": scores[ind].numpy().item(),
            "segmentation": [],
        }  # COCO json format
        data_list.append(pred_data)
