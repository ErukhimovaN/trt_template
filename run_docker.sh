#!/bin/bash
docker run --gpus all --env-file env_file -it --entrypoint /bin/bash \
    -v ~/nerukhimova/datasets/minival:/demo/datasets/minival \
    -v ~/nerukhimova/trt_cache:/tmp/trt_cache \
    -v ~/nerukhimova/modules:/root/.local/lib/python3.6/site-packages \
    trtinference:latest

