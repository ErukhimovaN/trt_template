import json
import logging
import pathlib

from typing import List, Optional, Tuple, Union

import cv2
import numpy as np
import torch
from torchvision import ops
import usb.core

LOG = logging.getLogger(__name__)

LOG = logging.getLogger(__name__)

def get_intersection(xyxy1, xyxy2):
    dx = min(xyxy1[2], xyxy2[2]) - max(xyxy1[0], xyxy2[0])
    dy = min(xyxy1[3], xyxy2[3]) - max(xyxy1[1], xyxy2[1])
    if dx > 0 and dy > 0:
        return dx * dy
    return 0


def pair_features(person, face, intersection, f_area, mean=None, std=None):
    # person - xyxy
    # face - xyxy
    px0, py0, px1, py1 = person
    pw = px1 - px0
    fx0, fy0, fx1, fy1 = face

    features = np.array([
        intersection / f_area,  # relative intersection
        (fx0 - px0) / pw,  # relative left distance from face to person
        (fy0 - py0) / pw,  # relative top distance from face to person
    ])
    if mean is not None:
        features -= mean
    if std is not None:
        features /= std
    return features


def linear_matcher(persons, faces, W, b, mean, std) -> Tuple[Tuple, Tuple]:
    f_areas = [(x[2] - x[0]) * (x[3] - x[1]) for x in faces]

    pairs = []
    person_ids = list(range(len(persons)))
    for f_id in reversed(np.argsort(f_areas)):
        face = faces[f_id]
        scores = []
        for p_id in person_ids:
            person = persons[p_id]
            intersection = get_intersection(person, face)
            if intersection == 0:
                continue
            score = W @ pair_features(person, face, intersection, f_areas[f_id], mean, std)
            scores.append(score)
        if not scores:
            continue
        score_id = np.argmax(scores)
        if scores[score_id] + b > 0:
            p_id = person_ids[score_id]
            pairs.append((p_id, f_id))
            person_ids.remove(p_id)
    if pairs:
        person_ids, face_ids = zip(*pairs)
    else:
        person_ids, face_ids = (), ()
    return person_ids, face_ids


def multiclass_nms_torch(boxes, scores, nms_thr, score_thr):
    scores, idx = scores.max(dim=-1)
    mask = scores > score_thr
    boxes = boxes[mask]
    scores = scores[mask]
    idx = idx[mask]
    keep = ops.batched_nms(boxes, scores, idx, nms_thr)
    return torch.cat([boxes[keep], scores[keep, None], idx[keep, None]], dim=-1)


def match_persons_faces(persons, faces) -> Tuple[Tuple, Tuple]:
    # persons - [xyxy]
    # faces - [xyxy]

    # Params tuned with linear regression for best person-face matching (above 99%)
    W = np.array([[12.01176766, -13.98395533, -15.3890675]])
    b = np.array([-28.21300884])
    mean = np.array([0.22480094, 1.31122743, 1.75737413])
    std = np.array([0.41060234, 6.5597941,  4.60115293])
    return linear_matcher(persons, faces, W, b, mean, std)


class IgnoreRegions:
    def __init__(self, areas: List[List[List]]) -> None:
        self.exclude_areas = [np.asarray(a) for a in areas]  # type: ignore

    @classmethod
    def from_json(cls, rois_path: Optional[str] = None) -> 'IgnoreRegions':
        if rois_path:
            with open(rois_path) as f:
                data = json.load(f)
            areas = []
            for k, v in data.items():
                if k.startswith('roi'):
                    areas.append(v)
        else:
            areas = []
        return cls(areas)

    def contains(self, bbox) -> bool:
        x0, y0, x1, y1 = map(int, bbox)
        inside_points = 0
        for area in self.exclude_areas:
            for x in (x0, x1):
                for y in (y0, y1):
                    if cv2.pointPolygonTest(area, (x, y), measureDist=False) > 0:
                        inside_points += 1
        return inside_points == 4

def create_heartbeat(run_file):
    run_file = pathlib.Path(run_file)

    def f(run_file=run_file):
        run_file.touch()
    return f


def reset_oak():
    dev = usb.core.find(idVendor=0x03e7)
    if dev is None:
        raise RuntimeError('OAK is not found')
    LOG.info("Resetting OAK")
    dev.reset()


def safe_crop_frame(image: torch.Tensor, bbox: Union[torch.Tensor, np.ndarray]) -> torch.Tensor:
    x0, y0, x1, y1 = map(int, bbox)
    x0 = max(0, x0)
    y0 = max(0, y0)
    return image[:, y0: y1, x0: x1]
