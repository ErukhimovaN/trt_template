# Host installation

Create and activate virtual enviroment:
```
python3 -m venv /path/to/env
/path/to/env/bin/activate
```

Install python packages:
```
pip install -r requirements.txt
```

Download TensorRT: [Link](https://developer.nvidia.com/nvidia-tensorrt-download).Registration is required.

Install TensorRT. Follow NVIDIA installation [instruction](https://docs.nvidia.com/deeplearning/tensorrt/install-guide/index.html#installing). Recommended way is the
[tar file](https://docs.nvidia.com/deeplearning/tensorrt/install-guide/index.html#installing-tar) installation. For most purposes, it's enough to pass only 5 steps from the instruction.

Convert ONNX model to TensorRT:

```
/tensorrt/absolute/path/TensorRT-version/bin/trtexec --onnx=path/to/model.onnx --saveEngine=path/to/model_fp16.trt --fp16
```
To support batching, add `--explicitBatch` flag to the command above and spesify shapes:
```
/tensorrt/absolute/path/TensorRT-version/bin/trtexec --onnx=path/to/model.onnx --saveEngine=path/to/engine.trt --fp16 --explicitBatch --minShapes=<input_name>:1x3x416x416 --optShapes=<input_name>:1x3x416x416 --maxShapes=<input_name>:4x3x416x416
```

ONNX model must be converted with dynamic shapes.

If no TRT engine is provided, onnx model will be converted automatically by trt_inference.py

Run [trt_inference.py](./trt_inference.py) file with the following flags:

    --model_path - path to your onnx model/trt engine
    --dataset_dir - path to folder with images
    --annotation_dir - path to file with annotations
    --output_dir - folder for lables images
    --batch_size
    --conf_thr
    --nms_thr

# Docker for host

Build docker image:
```
docker build -t trtinference:1.0 -f docker/host.Dockerfile /path/to/workdir
```
Modify `env_file` according to your needs.

Run docker container:
```
export DOCKER_DATASET_PATH=/path/to/host/dataset
docker run --gpus all --env-file env_file -it --rm --entrypoint /bin/bash -v $DOCKER_DATASET_PATH:/demo/datasets/minival -v /tmp/trt_cache:/tmp/trt_cache trtinference:1.0
```
Run demo:
```
python3 trt_inference.py
```
# Docker for jetson

Build docker image:
```
docker build -t trtinference:latest -f docker/jetson.Dockerfile /path/to/workdir
```

Please customize the `run_docker.sh` and `env_file` files to suit your requirements. Ensure that you have configured the folder containing the TensorRT and torchvision modules as a volume for the Docker container.

Run docker container:
```
cd yolox_trt
./run_docker.sh
```
