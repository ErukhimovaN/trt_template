import argparse
import json
import os
from pathlib import Path
import time

import cv2
import numpy as np
import torch

from trt_models import YoloXNanoModel
from evaluator import (
    evaluate_prediction,
    convert_to_coco_format
)
from coco_classes import COCO_CLASSES
from utils.metric import AverageMeter
from utils.visualize import vis

def parse_args() -> argparse.Namespace:
    MODEL_PATH = os.getenv('MODEL_PATH')
    DATASET_DIR = os.getenv('DATASET_DIR')
    OUTPUT_DIR = os.getenv('OUTPUT_DIR')
    ANNOTATION_DIR = os.getenv('ANNOTATION_DIR')
    BATCH_SIZE = os.getenv('BATCH_SIZE', 1)
    CONF_THR = os.getenv('CONF_THR',0.01)
    NMS_THR = os.getenv('NMS_THR',0.65)

    parser = argparse.ArgumentParser()
    parser.add_argument("--model_path", type=str, default=MODEL_PATH)
    parser.add_argument("--dataset_dir", type=str, default=DATASET_DIR)
    parser.add_argument("--output_dir", type=str, default=OUTPUT_DIR)
    parser.add_argument("--annotation_dir", type=str, default=ANNOTATION_DIR)
    parser.add_argument("--batch_size", type=int, default=BATCH_SIZE)
    parser.add_argument("--conf_thr", type = float, default = CONF_THR)
    parser.add_argument("--nms_thr", type = float, default = NMS_THR)
    return parser.parse_args()

#In case you need to save output tensor from network
def save_tensor(tensor,output_name):
        if image_path==Path("datasets/minival/minival/000000034873.jpg"):
            tensor_j = tensor.clone()
            tensor_j = tensor_j.to("cpu")
            output_path = "/tmp/trt_cache/"+output_name+"34873"+".pt"
            torch.save(tensor_j,output_path)


am_preproc = AverageMeter()
am_main = AverageMeter()
am_inference = AverageMeter()
am_nms = AverageMeter()
am_vis = AverageMeter()

args = parse_args()
model_path = Path(args.model_path)
dataset_dir = Path(args.dataset_dir)
output_dir = Path(args.output_dir)
path_to_annotation = Path(args.annotation_dir)
image_pattern = '*.jpg'
image_paths = list(dataset_dir.glob(image_pattern))
os.makedirs(args.output_dir, exist_ok=True)
batch_size = args.batch_size
conf_thr = args.conf_thr
nms_thr = args.nms_thr

cls_names=COCO_CLASSES
print(path_to_annotation)
print(args.annotation_dir)
with open(path_to_annotation, "r") as f:
    annotations_data = json.load(f)
    
pred_coco_format = []

model = YoloXNanoModel(model_path, batch_size=batch_size, nms_thr = nms_thr,score_thr=conf_thr)
print('model initialised')

# If last batch is incomplete, images from the begining of the list are added 
last_batch_add = batch_size - len(image_paths)%batch_size
if last_batch_add!=batch_size:
    image_paths +=image_paths[0:last_batch_add]

batches_list = [image_paths[i:i+batch_size] for i in range(0, len(image_paths), batch_size)]

for batch in batches_list:
    start_main = time.time()
    preprocessed = []
    orig_images = []
    ratios = []

    for image_path in batch:
        start = time.time()
        image = cv2.imread(str(image_path))
        orig_images.append(image.copy())
        image = np.transpose(image,[2,0,1])
        image = torch.from_numpy(image).float().to('cuda')

        image, ratio = model.preprocess(image)

        preprocessed.append(image)
        ratios.append(ratio)
        am_preproc.update(time.time() - start)

    preprocessed = torch.stack(preprocessed)
    preprocessed = preprocessed.squeeze(1)

    torch.cuda.synchronize()

    start = time.time()
    inferred = model.inference(preprocessed)
    am_inference.update(time.time() - start)

    start = time.time()
    predictions = model.postprocess(inferred)
    am_nms.update(time.time() - start)

    for idx,prediction in enumerate(predictions):
        start = time.time()
        prediction = prediction.to('cpu')
        bboxes = prediction[:, 0:4]*ratios[idx]
        cls = prediction[:, 6]
        scores = prediction[:, 4] * prediction[:, 5]

        vis_res = vis(orig_images[idx], bboxes, scores, cls, 0, cls_names)

        output_path = str(output_dir/batch[idx].name)
        cv2.imwrite(output_path, vis_res)
        am_vis.update(time.time() - start)

        convert_to_coco_format(pred_coco_format, bboxes, scores, cls, batch[idx], annotations_data)

    am_main.update(time.time() - start_main)

(ap50_95, ap50) = evaluate_prediction(pred_coco_format, path_to_annotation)

print(f"Preprocess: {round(am_preproc.median*1000)} ms")
print(f"Inference: {round(am_inference.median*1000/batch_size)} ms")
print(f"NMS: {round(am_nms.median*1000/batch_size)} ms")
print(f"Visualization: {round(am_vis.median*1000)} ms")
print(f"Overall time: {round(am_main.median*1000/batch_size)} ms")

print(f"AP50_95: {round(ap50_95,2)}")
print(f"AP50: {round(ap50,2)}")
